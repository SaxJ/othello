import turtle
import math
import random

'''
The side length of a grid square in pixels.
'''
CELL_DIM = 50


def initialiseBoard(n):
    '''
    Returns the initial othello board of size n
    '''
    if n % 2 != 0:
        raise Exception('Board size must be an even number')

    half = n // 2
    board = []
    for i in range(n):
        board.append([])
        for j in range(n):
            k = 0
            if i == half-1 and j == half-1 or i == half and j == half:
                k = 1
            if i == half and j == half-1 or i == half-1 and j == half:
                k = -1

            board[i].append(k)

    return board


def drawSquare(x, y, dim, fillColor):
    '''
    Draws a dim x dim square with bottom-left corner at coords (x, y).
    follColor is the color filled into the square.
    '''
    turtle.goto(x, y)
    turtle.setheading(90)
    turtle.down()
    turtle.fillcolor(fillColor)
    turtle.begin_fill()

    for l in range(4):
        turtle.forward(dim)
        turtle.right(90)

    turtle.end_fill()
    turtle.up()


def drawPiece(board, row, col):
    '''
    Draws a circle of the correct color and at the correct coordinates
    to represent a peice at (row, col) on the board.
    '''
    d = len(board)
    width = CELL_DIM * d
    origin = -width / 2

    positionX = origin + col * CELL_DIM + CELL_DIM / 2
    positionY = origin + row * CELL_DIM

    color = 'black' if board[row][col] > 0 else 'white'

    turtle.fillcolor(color)
    turtle.begin_fill()
    turtle.goto(positionX, positionY)
    turtle.circle(CELL_DIM / 2)
    turtle.end_fill()


def drawGrid(b):
    '''
    Draws a grid of lines to mark the squares on the board b.
    '''
    dim = CELL_DIM * len(b)
    drawSquare(-dim / 2, -dim / 2, dim, 'green')
    for r in range(len(b)):
        turtle.up()
        turtle.setheading(90)
        turtle.goto(-dim / 2 + CELL_DIM * r, -dim / 2)
        turtle.down()
        turtle.forward(dim)
        turtle.up()
        turtle.setheading(0)
        turtle.goto(-dim / 2, -dim / 2 + CELL_DIM * r)
        turtle.down()
        turtle.forward(dim)
    turtle.up()


def drawBoard(b):
    '''
    Draws the peices on the board represented by b.
    '''
    turtle.speed(500)
    d = range(len(b))
    for r in d:
        for c in d:
            if b[r][c] != 0:
                drawPiece(b, r, c)


def isThatPeice(me, other):
    '''
    Returns Truw if me and other are the pieces of the same color, and False
    otherwise.
    '''
    if me < 0:
        return other < 0
    elif me > 0:
        return other > 0
    else:
        return other == 0


def isInBounds(b, x, y):
    '''
    Returns True if (x, y) is within the bounds of the board b, and
    False otherwise.
    '''
    return not (x >= len(b) or x < 0 or y < 0 or y >= len(b))


def move(b, m, p):
    """
    Move m is represented by (r, c, ms) where r, c are row, column
    and ms is list of capture directions.
    p is the player eg. 1 black, -1 white
    """
    startRow, startCol, dirs = m
    b[startRow][startCol] = p
    for direction in dirs:
        cy, cx = direction
        pr = cy + startRow
        pc = cx + startCol
        while isInBounds(b, pc, pr) and not isThatPeice(b[pr][pc], p) and b[pr][pc] != 0:
            b[pr][pc] = p * (int(math.fabs(b[pr][pc])) + 1)
            pr += cy
            pc += cx

    drawBoard(b)
    return b


def legalDirection(r, c, b, p, dr, dc):
    """
    Returns True if a piece places by player p on square (r, c)
    on board b would capture any pieces in direction (dr, dc)
    """
    nextR = dr + r
    nextC = dc + c
    matchingPieceFound = False
    differentPieceFound = False
    while isInBounds(b, nextR, nextC):
        square = b[nextR][nextC]
        if square == 0:
            break
        elif isThatPeice(p, square):
            matchingPieceFound = True
            break
        else:
            differentPieceFound = True

        nextR += dr
        nextC += dc

    return (matchingPieceFound and differentPieceFound)


def legalMove(r, c, b, p):
    """
    Returns a list of directions in which a piece placed on board b by
    player p at (r, c) would capture.
    If the move id not legal, [] is returned.
    """
    moves = []
    for i in [-1, 0, 1]:
        for j in [-1, 0, 1]:
            if i == 0 and j == 0:
                continue

            if legalDirection(r, c, b, p, i, j) and b[r][c] == 0:
                moves.append((i, j))
    return moves


def moves(b, p):
    """
    Returns a list of legal moves that can be made by player p
    on board b.
    """
    moves = []
    for r in range(len(b)):
        for c in range(len(b)):
            dirs = legalMove(r, c, b, p)
            if len(dirs) > 0:
                moves.append((r, c, dirs))
    return moves


def gradeMove(b, m, p):
    """
    Returns the score of the move m made by player p on board b.
    """
    startRow, startCol, dirs = m
    count = p
    for direction in dirs:
        cy, cx = direction
        pr = cy + startRow
        pc = cx + startCol
        while isInBounds(b, pc, pr) and not isThatPeice(b[pc][pr], p):
            count += math.fabs(1 + math.fabs(b[pr][pc]))
            pr += cy
            pc += cx

    return count


def selectMove(moves, b, p):
    """
    Selects the move in moves with the largest 'gradeMove' value.
    """
    bestIndex = -1
    bestValue = -1
    for i in range(len(moves)):
        move = moves[i]
        val = gradeMove(b, move, p)
        if val > bestValue:
            bestValue = val
            bestIndex = i

    return moves[bestIndex]


def scoreBoard(b):
    """
    Returns the scores of black and white as a pair (blackScore, whiteScore).
    """
    blackSum, whiteSum = (0, 0)
    for row in b:
        for col in row:
            if col > 0:
                blackSum += col
            elif col < 0:
                whiteSum += math.fabs(col)

    return (int(blackSum), int(whiteSum))


def toBoardCoords(b, x, y):
    """
    Converts (x, y) coords of the drawing plane to (row, col) coords to
    be used on the board b.
    """
    boardWidth = len(b) * CELL_DIM
    x += boardWidth / 2
    y += boardWidth / 2

    (c, r) = (int(x // CELL_DIM), int(y // CELL_DIM))
    return (c, r)


def displayWinner(b):
    turtle.clear()
    turtle.goto(0, 0)
    turtle.fillcolor('black')

    blackScore, whiteScore = scoreBoard(b)
    text = "It's a tie!"
    if blackScore > whiteScore:
        text = "Black Wins!"
    elif whiteScore > blackScore:
        text = "White Wins!"

    turtle.write(text, move=False, align="Center", font=("Arial", 22, "normal"))


def onClickCreate(p, board):
    """
    Returns a function that will handle clicks on the canvas made by player p.
    Handler handles a click on the turtle drawing at co-ords (x, y).
    When a click is made, the move is checked for legality. If the
    move is legal, the move is made and the computer will respond with its move.
    Is a closure holding the current player and board.
    """
    def handler(x, y):
        (col, row) = toBoardCoords(board, x, y)
        possibleMoves = moves(board, p)
        playerSkip = True

        chosenMove = [(r, c, mvs) for (r, c, mvs)
                      in possibleMoves if r == row and c == col]
        if len(chosenMove) > 0:
            # Then the click has a valid move, go ahead and make it.
            move(board, chosenMove[0], p)
            playerSkip = False

        # Now the computer will select it's move
        legalMoves = moves(board, -p)
        if len(legalMoves) == 0 and playerSkip:
            displayWinner(board)
        elif len(legalMoves) > 0:
            aiMove = selectMove(legalMoves, board, -p)
            # Go ahead Mr Computer
            move(board, aiMove, -p)

    return handler


def main():
    boardSize = int(input("Greetings! What board size would you like?"))
    turtle.speed(10)
    turtle.hideturtle()
    b = initialiseBoard(boardSize)

    # Randomly slect player color.
    player = random.choice([-1, 1])
    if player == -1:
        legalMoves = moves(b, -player)
        aiMove = selectMove(legalMoves, b, -player)
        move(b, aiMove, -player)

    # Draw the grid for the board.
    drawGrid(b)
    drawBoard(b)

    # Create and assign the click handler.
    turtle.onscreenclick(onClickCreate(player, b))

    # Loop until the game ends
    input()

if __name__ == "__main__":
    main()
